$(document).ready(function() {
    bindingUserData();
    getSavedData();
    
});
function setDeleteListen() {
    $(".delete").click(function(event) {
        event.preventDefault();
        let saveId = $(this).attr("saveId");
        console.log(saveId);
        setDeleteAction(saveId);
    });
}
function setDeleteAction(saveId) {
    $.ajax({
        type: "DELETE",
        url: "/secure_api/diagnosis/save/" + saveId
    }).done(data => {
        console.log("delete ok: " + data);
        window.location.reload();
    }).fail(error => {
        console.log("delete error: " + error);
    });
}
function getSavedData() {
    $.ajax({
        type: "GET",
        url: "/secure_api/diagnosis/saved"
    }).done(data => {
        console.log("ok: " + data);
        data = JSON.parse(data);
        bindingSavedData(data);
        setDeleteListen();
    }).fail(error => {
        console.log("error: " + error);
    });
}

function bindingSavedData(data) {
    $(".emp-profile #diagnosisedNum").text(data.length);
    for (let i = 0; i < data.length; i++) {
        let savedEle = getSaveEle();
        setDataForSaveEle(data[i], savedEle);
        $("#savedList tbody").append(savedEle);
    }
}

function bindingUserData() {
    $(".emp-profile #name").text(sessionStorage.getItem("userName"));
    $(".emp-profile #email").text(sessionStorage.getItem("email"));
    $(".emp-profile #startedAt").text(getDateFromStr(sessionStorage.getItem("startedAt")));
    $(".emp-profile .profile-img img").attr("src", sessionStorage.getItem("profilePic"));
}

function setDataForSaveEle(data, ele) {
    $(ele).attr("saveId", data.id);
    $(ele).find("a.delete").attr("saveId", data.id);
    $(ele).find(".date").text(getDateFromStr(data.savedTime));
    let symptomStr = data.symptoms;
    let array = symptomStr.split(";");
    for (let i = 0; i < array.length; i++) {
        let symptomEle = getSymptomEle();
        $(symptomEle).text(array[i]);
        $(ele).find(".symptoms").append(symptomEle);
    }
    let diaseaseStr = data.diaseases;
    let diaseaseArr = diaseaseStr.split(";");
    for (let i = 0; i < diaseaseArr.length - 1; i++) {
        let subArr = diaseaseArr[i].split(":");
        console.log(subArr[1] + ": " + subArr[2] + "%");
        let diaseaseEle = getDiaseaseEle();
        $(diaseaseEle).text(subArr[1] + ": " + subArr[2] + "%");
        $(ele).find(".diaseases").append(diaseaseEle);
    }

}

function getDiaseaseEle() {
    return $("tbody .saved-ele.hide .diasease-ele.hide").clone().removeClass("hide");
}

function getSymptomEle() {
    return $("tbody .saved-ele.hide .symptom-ele.hide").clone().removeClass("hide");
}
function getSaveEle() {
    return $("#savedList .saved-ele.hide").clone().removeClass("hide");
}

function getDateFromStr(str) {
    let date = new Date(str);
    let result =  date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();
    console.log("date: " + result); 
    return result;
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/; domain=.benhthuysan.vn";
}

function deleteCookie(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/; domain=.benhthuysan.vn";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}
