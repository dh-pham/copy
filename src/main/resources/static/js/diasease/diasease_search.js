const numAndClassStar = {
	"0": "far fa-star star-mark",
	"0.5": "fas fa-star-half-alt star-mark",
	"1": "fas fa-star star-mark"
};

$(document).ready(function(e){
	bindingDataFromUrl();
	$("#search-box button").on("click", function(e) {
		e.preventDefault();
		goSearching();
	});
	
	$("#text-input").keypress(function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			goSearching();
		}
	});
	
});

function bindingDataFromUrl() {
	let urlParams = new URLSearchParams(window.location.search);
	let keyword = urlParams.get("q");
	let pageIndex = urlParams.get("page");
	let urlTarget = generateApiUrl(keyword, pageIndex);
	console.log(urlTarget);
	$.ajax({
		type: "GET",
		url: urlTarget
	}).done(function(data) {
		// console.log(data);
		let content = data.content;
		$("#text-input").val(keyword);
		$("#text-input").focus();
		$("#result-amount").text(data.totalElements);
		let diaseaseList = $("#result-list");
		
		for (let i = 0; i < content.length; i++) {
			let diaseaseEle = createDiaseaseHtmlEleFromData(content[i]);
			$(diaseaseList).append(diaseaseEle);
		}
		if (keyword != null) {
			highlightKeyword(keyword);
		}
		if (data.totalPages != 0) {
			let paginationEle = createPagination(data.number, data.totalPages, data.first, data.last, keyword);
			$(".pagination.hide").after(paginationEle);
		}
	}).fail(function(error) {
		console.log(error);
	});
}

function getDiaseaseClone() {
	return $("#result-list .search-result[class~='hide']").clone().removeClass("hide");
}

function createDiaseaseHtmlEleFromData(data) {
	console.log(data.diaseaseName);
	let eleEle = getDiaseaseClone();
	$(eleEle).attr("id", "result-" + data.id);
	$(eleEle).find(".diasease-name").text(data.diaseaseName);
	$(eleEle).find(".diasease-href").attr("href", generateDetailPageUrl(data.id));
	$(eleEle).find(".image-area").attr("src", data.image);
	$(eleEle).find(".review-amount").text(data.reviewerAmount);
	$(eleEle).find(".review-score").text(data.scoreAvg);
	$(eleEle).find(".seaFood-name").text(data.seaFood.seaFoodName);
	$(eleEle).find(".diasease-type").text(data.diaseaseType.diaseaseTypeName);
	$(eleEle).find(".treatment").text(data.treatment.substring(0, 153));
	setStarRatingEle(data.scoreAvg, $(eleEle).find(".comment-star"));
	return eleEle;
}

function getKeywordFromTextBox() {
	let keyword = $("#text-input").val().trim();
	console.log("text-input: " + keyword);
	if (keyword == "") return null;
	return keyword;
}

function goSearching() {
	let keyword = getKeywordFromTextBox();
	window.location.href = generatePageUrl(keyword, 0);
}

function highlightKeyword(keyword) {
	let reg = new RegExp(keyword, 'gi');
	let htmlCode = $(".diasease-name, .seaFood-name").html();
	$(".diasease-name, .seaFood-name").each(function() {
		let htmlCode = $(this).html();
		let htmlHighlight = htmlCode.replace(reg, function() {
			return `<span class='highlight'>${keyword}</span>`;
		});
		$(this).html(htmlHighlight);
	});
}

function createPagination(current, total, isFirst, isLast, keySearch) {
	let paginationClone = createPaginationClone();
	$(paginationClone).find(".page-num").remove();
	for(let i = 0; i < total; i++) {
		let pageNumEleClone = createPageNumClone();
		$(pageNumEleClone).text(i+1);
		let url = generatePageUrl(keySearch, i);
		$(pageNumEleClone).attr("href", url);
		if (current == i) {
			$(pageNumEleClone).addClass("active");
		}
		$(paginationClone).find(".next-page").before(pageNumEleClone);	
	}
	let prevUrl = generatePageUrl(keySearch, current - 1);
	let nextUrl = generatePageUrl(keySearch, current + 1);
	$(paginationClone).find(".prev-page").attr("href", prevUrl);
	$(paginationClone).find(".next-page").attr("href", nextUrl);
	if (isFirst == true) {
		$(paginationClone).find(".prev-page").addClass("hide");
	}
	if (isLast == true) {
		$(paginationClone).find(".next-page").addClass("hide");
	}
	return paginationClone;

}

function createPaginationClone() {
	return $(".pagination:first").clone().removeClass("hide");
}

function createPageNumClone() {
	return $(".page-num:first").clone().removeClass("hide");
}

function generatePageUrl(keyword, pageIndex) {
	if (pageIndex == null) pageIndex = 0;
	if (keyword == null) {
		return `/diaseases?page=${pageIndex}`;
	} else {
		return `/diaseases?q=${keyword}&page=${pageIndex}`;
	}
}

function generateApiUrl(keyword, pageIndex) {
	if (pageIndex == null) pageIndex = 0;
	if (keyword == null) {
		return `/api/diaseases?page=${pageIndex}`;
	} else {
		return `/api/diaseases?q=${keyword}&page=${pageIndex}`;
	}
}

function generateDetailPageUrl(diaseaseId) {
	return `/diaseases/${diaseaseId}`;
}

function setStarRatingEle(rating_num, ele) {
	$(ele).find("i").each(function() {
		let num = getStarNum(this);
		setStarClass(computeClassCode(num, rating_num), this);
	});
}

function getStarNum(ele) {
	return parseInt($(ele).attr("starnum"));
}

// 1-> "fas far-star", 0.5 -> "fas fa-star-half-alt", 0 -> "far far-star"
function setStarClass(classCode, ele) {
	$(ele).attr("class", numAndClassStar[classCode]);
}

function computeClassCode(num, rating_num) {
	if (num <= rating_num) {
		return "1";
	} else if (num < rating_num + 1) {
		return "0.5";
	} else {
		return "0";
	}
}