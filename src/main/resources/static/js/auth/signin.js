$(document).ready(function() {
    let urlParams = new URLSearchParams(window.location.search);
    console.log(urlParams.toString());
    if (urlParams.get("error") != null) {
        $("#error-message").css("display", "block");
        $("form > input").css("border-color", "red");
    }    
});
