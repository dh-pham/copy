$(document).ready(function() {
	$("#submit-btn").on("click", function(e) {
		e.preventDefault();
		$("input").css("border-color", "rgb(104, 145, 162)")
		let yourname = $("#inputYourname").val();
		let email = $("#inputEmail").val();
		let password = $("#inputPassword").val();
		let confirmPassword = $("#inputConfirmPassword").val();
		// check your name and password
		if (validateInput(yourname, email, password, confirmPassword)) {
			let sendJson = {};
			sendJson['yourname'] = yourname;
			sendJson['email'] = email;
			sendJson['password'] = password;
			
			$.ajax({
				type: "POST",
				url: "/signup",
				contentType: "application/json",
				data: JSON.stringify(sendJson),
			}).done(function(data, statusText, xhr) {
				console.log("ok: " + JSON.stringify(data));
				console.log(statusText);
				console.log(xhr);
				let alertEle = $(".custom-alert");
				$(alertEle).find(".alert-message").text("Đăng ký thành công!");
				$(alertEle).removeClass("hide");
				setTimeout(function() {
					window.location.href = "/signin";
				}, 2500);
			}).fail(function(xhr) {
//				console.log(JSON.stringify(error));
//				console.log(statusText);
				console.log(xhr);
				let responseText = JSON.parse(xhr.responseText);
				if (xhr.status === 409) {
					showError(responseText.email, "#inputEmail");
				}
			});
		}
	});
});

function validateInput(yourname, email, password, confirmPassword) {
	let yournameSelector = "#inputYourname";
	let emailSelector = "#inputEmail";
	let passwordSelector = "#inputPassword";
	let confirmPasswordSelector = "#inputConfirmPassword";
	console.log("validating...");
	if (/\d/.test(yourname) === true || yourname.length === 0) {
		showError("Họ tên không chứa kí tự đặc biệt và không để trống", yournameSelector);
		return false;
	} else if(validateEmail(email) === false) {
		showError("Email không hợp lệ.", emailSelector);
	} else if(password.length < 8) {
		showError("Mật khẩu nên chứa ít nhất 8 kí tự.", passwordSelector);
		return false;
	} else if(password !== confirmPassword) {
		showError("Mật khẩu xác nhận không khớp.", passwordSelector);
		showError("Mật khẩu xác nhận không khớp.", confirmPasswordSelector);
		return false;
	} else {
		return true;
	}
}

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

function showError(message, selector) {
	$("#error-message label").text("* " + message);
	$(selector).css("border-color", "#FF2D00");
}