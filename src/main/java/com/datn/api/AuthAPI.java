package com.datn.api;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.auth.AppUser;
import com.datn.service.auth.IUserDetailsService;

@RestController
@RequestMapping("/api")
public class AuthAPI {
	
	@Autowired
	private IUserDetailsService userService;
	
	@GetMapping("/getMyInfo")
	public AppUser getAppUser(Principal principal) {
		return userService.findByEmail(principal.getName());
	}
}
