package com.datn.utils.auth;

import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtil {


	public static Cookie generateNewCookie(String name, String value, int maxAge) {

		String domain = "benhthuysan.vn";

		Cookie cookie = new Cookie(name, value);
		cookie.setDomain(domain);
		cookie.setPath("/");
		cookie.setMaxAge(maxAge);

		return cookie;
	}

	public static Optional<String> getCookie(String cookieName, HttpServletRequest request) {

		if (request.getCookies() != null && request.getCookies().length != 0) {
			for (Cookie cookie : request.getCookies()) {
				String name = cookie.getName();
				if (name.compareTo(cookieName) == 0) {
					String value = cookie.getValue();
					return Optional.of(value);
				}
			}
		}

		return Optional.empty();
	}

//	public static Optional<String> getCookies(HttpServletRequest request) {
//
//		if (request.getCookies() != null && request.getCookies().length != 0) {
//			String cookies = Arrays.asList(request.getCookies()).stream().map(cookie -> {
//				return String.format("%s: %s", cookie.getName(), cookie.getValue());
//			}).collect(Collectors.joining(", "));
//			return Optional.of(cookies);
//
//		}
//
//		return Optional.ofNullable(null);
//	}
}
