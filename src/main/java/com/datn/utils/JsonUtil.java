package com.datn.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {

	public static JsonNode getJsonNodeFromStr(String jsonStr) throws IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(jsonStr);
	}
	public static String convertObjToStr(Object o) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(o);
	}

}
