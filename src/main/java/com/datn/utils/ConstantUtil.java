package com.datn.utils;

public class ConstantUtil {

	public static final String DATA_DIR = "./data/";
	public static final String TRAINING_DIR = "./data/trainingSet/";
	public static final String TESTING_DIR = "./data/testingSet/";
	public static final String ORIGIN_DIR = "./data/originSet/";
	public static final Integer MIN = -99999999;
}
