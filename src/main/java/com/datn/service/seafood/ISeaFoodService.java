package com.datn.service.seafood;

import java.util.List;
import java.util.Optional;

import com.datn.entity.SeaFood;

public interface ISeaFoodService {
	public List<SeaFood> getAll();
	public Optional<SeaFood> getById(Integer id);
}
