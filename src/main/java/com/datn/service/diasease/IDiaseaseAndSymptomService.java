package com.datn.service.diasease;

import java.util.List;

import com.datn.entity.diasease.DiaseaseAndSymptom;

public interface IDiaseaseAndSymptomService {
	
	public List<DiaseaseAndSymptom> getBySeaFoodId(int seaFoodId);
}
