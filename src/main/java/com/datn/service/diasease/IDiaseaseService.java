package com.datn.service.diasease;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.datn.entity.diasease.Diasease;
import com.datn.entity.review.Review;

public interface IDiaseaseService {
	public Page<Diasease> getAll(Pageable pageable);
	public Optional<Diasease> getById(Integer id);
	public List<Diasease> searchByDiaseaseName(String diaseaseName);
	public List<Diasease> searchBySeaFoodName(String seaFoodName);
	public Page<Diasease> searchByKeyword(String keyword, Pageable pageable);
	public Page<Review> getReviews(Integer diaseaseId, Pageable pageable);
}
