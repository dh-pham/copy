package com.datn.service.diasease;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.datn.entity.diasease.Diasease;
import com.datn.entity.review.Review;
import com.datn.repository.diasease.DiaseaseRepository;
@Service
public class DiaseaseServiceImpl implements IDiaseaseService{
	public static final Logger logger = LoggerFactory.getLogger(DiaseaseServiceImpl.class);
	@Autowired
	private DiaseaseRepository diaseaseRepo;
	@Override
	public Page<Diasease> getAll(Pageable pageable) {
		return diaseaseRepo.findAll(pageable);
	}
	@Override
	public List<Diasease> searchByDiaseaseName(String diaseaseName) {
		String param = "%" + diaseaseName + "%";
		logger.debug(param);
		return diaseaseRepo.searchByDiaseaseName(param);
	}
	@Override
	public List<Diasease> searchBySeaFoodName(String seaFoodName) {
		String param = "%" + seaFoodName + "%";
		return diaseaseRepo.searchBySeaFoodName(param);
	}
	@Override
	public Optional<Diasease> getById(Integer id) {
		return diaseaseRepo.findById(id);
	}
	@Override
	public Page<Diasease> searchByKeyword(String keyword, Pageable pageable) {
		String param = "%" + keyword + "%";
		return diaseaseRepo.searchByKeyword(param, pageable);
	}
	@Override
	public Page<Review> getReviews(Integer diaseaseId, Pageable pageable) {
		return diaseaseRepo.getReviews(diaseaseId, pageable);
	}

}
