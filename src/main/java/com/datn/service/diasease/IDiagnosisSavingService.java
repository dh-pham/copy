package com.datn.service.diasease;

import java.util.List;
import java.util.Optional;

import com.datn.entity.diasease.DiagnosisSaving;

public interface IDiagnosisSavingService {
	public DiagnosisSaving save(DiagnosisSaving obj);
	public List<DiagnosisSaving> getByUserId(Long userId);
	public Optional<DiagnosisSaving> getById(Integer id);
	public void delete(DiagnosisSaving save);
}
