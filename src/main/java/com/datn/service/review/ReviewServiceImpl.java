package com.datn.service.review;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.review.Review;
import com.datn.repository.review.ReviewRepository;

@Service
public class ReviewServiceImpl implements IReviewService {
	@Autowired
	private ReviewRepository repo;
	
	@Override
	public Review update(Review review) {
		return repo.save(review);
	}

	@Override
	public Review create(Review review) {
		return repo.save(review);
	}
	
	@Override
	public void delete(Review review) {
		repo.delete(review);
	}

	@Override
	public Optional<Review> getById(Integer id) {
		return repo.findById(id);
	}

}
