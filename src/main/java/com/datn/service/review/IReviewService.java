package com.datn.service.review;

import java.util.Optional;

import com.datn.entity.review.Review;

public interface IReviewService {
	public Review update(Review review);
	public Review create(Review review);
	public void delete(Review review);
	public Optional<Review> getById(Integer id);
}
