package com.datn.service.review;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.review.LikeReview;
import com.datn.repository.review.LikeReviewRepository;

@Service
public class LikeReviewServiceImpl implements ILikeReviewService {

	@Autowired
	private LikeReviewRepository repo;
	
	@Override
	public LikeReview create(LikeReview likeReview) {
		return repo.save(likeReview);
	}

	@Override
	public Optional<LikeReview> getByReviewAndUser(Integer reviewId, Long userId) {
		return repo.findByReviewAndUser(reviewId, userId);
	}

	@Override
	public void delete(LikeReview likeReview) {
		repo.delete(likeReview);
	}

	@Override
	public Optional<LikeReview> getById(Integer id) {
		return repo.findById(id);
	}

	@Override
	public List<LikeReview> getByDiaseaseAndUser(Integer diaseaseId, Long userId) {
		return repo.findByDiaseaseAndUser(diaseaseId, userId);
	}

}
