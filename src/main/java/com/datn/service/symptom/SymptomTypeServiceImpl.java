package com.datn.service.symptom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.symptom.SymptomType;
import com.datn.repository.symptom.SymptomTypeRepository;
@Service
public class SymptomTypeServiceImpl implements ISymptomTypeService{
	@Autowired
	private SymptomTypeRepository symptomTypeRepo;
	@Override
	public List<SymptomType> getAll() {
		return symptomTypeRepo.findAll();
	}

}
