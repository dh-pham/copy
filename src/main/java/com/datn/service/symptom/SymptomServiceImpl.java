package com.datn.service.symptom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.symptom.Symptom;
import com.datn.repository.symptom.SymptomRepository;

@Service
public class SymptomServiceImpl implements ISymptomService {
	@Autowired
	private SymptomRepository symptomRepo;
	@Override
	public List<Symptom> getAll() {
		return symptomRepo.findAll();
	}

}
