package com.datn.service.symptom;

import java.util.List;

import com.datn.entity.symptom.Symptom;

public interface ISymptomService {
	public List<Symptom> getAll();
}
