package com.datn.repository.diasease;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datn.entity.diasease.DiaseaseType;

@Repository
public interface DiaseaseTypeRepository extends JpaRepository<DiaseaseType, Integer>{

}
