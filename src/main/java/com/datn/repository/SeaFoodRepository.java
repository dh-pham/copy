package com.datn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datn.entity.SeaFood;

@Repository
public interface SeaFoodRepository extends JpaRepository<SeaFood, Integer>{

}
