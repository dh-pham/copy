package com.datn.repository.review;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.datn.entity.review.LikeReview;

@Repository
public interface LikeReviewRepository extends JpaRepository<LikeReview, Integer>{

	@Query("SELECT l FROM LikeReview l WHERE l.review.id = ?1 AND l.appUser.id = ?2")
	public Optional<LikeReview> findByReviewAndUser(Integer reviewId, Long userId);
	
	@Query("SELECT l FROM LikeReview l WHERE l.review.diasease.id = ?1 AND l.appUser.id = ?2")
	public List<LikeReview> findByDiaseaseAndUser(Integer diaseaseId, Long userId);
}
