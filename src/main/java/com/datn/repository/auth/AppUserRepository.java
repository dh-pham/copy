package com.datn.repository.auth;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datn.entity.auth.AppUser;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long>{

	public Optional<AppUser> findByName(String name);
	public Optional<AppUser> findByEmail(String email);
	
}
