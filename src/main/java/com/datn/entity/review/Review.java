package com.datn.entity.review;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.datn.entity.auth.AppUser;
import com.datn.entity.diasease.Diasease;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "review")
public class Review {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	private AppUser appUser;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "diaseaseId")
	private Diasease diasease;
	
	@Column(name = "score")
	private Integer score;
	
	@Column(name = "comment")
	private String comment;
	
	@Column(name = "time")
	private Timestamp time = new Timestamp(new Date().getTime());
	
	@JsonBackReference
	@OneToMany(mappedBy = "review", fetch = FetchType.LAZY)
	private List<LikeReview> likeReviews;

	@Column(name = "likedAmount")
	private Integer likedAmount = 0;
	
	public Review() {
		
	}
	
	public Review(AppUser appUser, Diasease diasease, Integer score, String comment) {
		super();
		this.appUser = appUser;
		this.diasease = diasease;
		this.score = score;
		this.comment = comment;
	}


	public Integer getLikedAmount() {
		return likedAmount;
	}

	public void setLikedAmount(Integer likedAmount) {
		this.likedAmount = likedAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonManagedReference
	public AppUser getAppUser() {
		return appUser;
	}

	@JsonManagedReference
	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	@JsonManagedReference
	public Diasease getDiasease() {
		return diasease;
	}

	@JsonManagedReference
	public void setDiasease(Diasease diasease) {
		this.diasease = diasease;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}
	
	@JsonBackReference
	public List<LikeReview> getLikeReviews() {
		return likeReviews;
	}

	@JsonBackReference
	public void setLikeReviews(List<LikeReview> likeReviews) {
		this.likeReviews = likeReviews;
	}
	
	
}
