package com.datn.entity.diasease;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.datn.entity.SeaFood;
import com.datn.entity.auth.AppUser;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "diagnosisSaving")
public class DiagnosisSaving {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	private AppUser appUser;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "seaFoodId")
	private SeaFood seaFood;
	
	@Column(name = "symptoms")
	private String symptoms;
	
	@Column(name = "diaseases")
	private String diaseases;
	
	@Column(name = "savedTime")
	private Timestamp savedTime = new Timestamp(new Date().getTime());

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonManagedReference
	public AppUser getAppUser() {
		return appUser;
	}

	@JsonManagedReference
	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	@JsonManagedReference
	public SeaFood getSeaFood() {
		return seaFood;
	}

	@JsonManagedReference
	public void setSeaFood(SeaFood seaFood) {
		this.seaFood = seaFood;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getDiaseases() {
		return diaseases;
	}

	public void setDiaseases(String diaseases) {
		this.diaseases = diaseases;
	}

	public Timestamp getSavedTime() {
		return savedTime;
	}

	public void setSavedTime(Timestamp savedTime) {
		this.savedTime = savedTime;
	}
	
	
}
