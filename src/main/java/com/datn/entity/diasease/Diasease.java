package com.datn.entity.diasease;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.datn.entity.SeaFood;
import com.datn.entity.review.Review;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="diasease")
public class Diasease {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name="seaFoodId")
	private SeaFood seaFood;
	
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name="diaseaseTypeId")
	private DiaseaseType diaseaseType;
	
	@Column(name="diaseaseCode")
	private String diaseaseCode;
	
	@Column(name="diaseaseName")
	private String diaseaseName;
	
	@Column(name="treatment")
	private String treatment;
	
	@Column(name="treatmentFile")
	private String treatmentFile;
	
	@Column(name="image")
	private String image;
	
	@Column(name="scoreAvg")
	private Float scoreAvg;
	
	@Column(name="reviewerAmount")
	private Integer reviewerAmount;
	
	@JsonBackReference
	@OneToMany(mappedBy="diasease", fetch = FetchType.LAZY)
	private List<DiaseaseAndSymptom> diaseaseAndSymptoms;

	@JsonBackReference
	@OneToMany(mappedBy = "diasease", fetch = FetchType.LAZY)
	private List<Review> reviews;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonManagedReference
	public SeaFood getSeaFood() {
		return seaFood;
	}

	@JsonManagedReference
	public void setSeaFood(SeaFood seaFood) {
		this.seaFood = seaFood;
	}

	@JsonManagedReference
	public DiaseaseType getDiaseaseType() {
		return diaseaseType;
	}

	@JsonManagedReference
	public void setDiaseaseType(DiaseaseType diaseaseType) {
		this.diaseaseType = diaseaseType;
	}

	public String getDiaseaseCode() {
		return diaseaseCode;
	}

	public void setDiaseaseCode(String diaseaseCode) {
		this.diaseaseCode = diaseaseCode;
	}

	public String getDiaseaseName() {
		return diaseaseName;
	}

	public void setDiaseaseName(String diaseaseName) {
		this.diaseaseName = diaseaseName;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public String getTreatmentFile() {
		return treatmentFile;
	}

	public void setTreatmentFile(String treatmentFile) {
		this.treatmentFile = treatmentFile;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Float getScoreAvg() {
		return scoreAvg;
	}

	public void setScoreAvg(Float scoreAvg) {
		this.scoreAvg = scoreAvg;
	}

	public Integer getReviewerAmount() {
		return reviewerAmount;
	}

	public void setReviewerAmount(Integer reviewerAmount) {
		this.reviewerAmount = reviewerAmount;
	}
	
	@JsonBackReference
	public List<DiaseaseAndSymptom> getDiaseaseAndSymptoms() {
		return diaseaseAndSymptoms;
	}
	
	@JsonBackReference
	public void setDiaseaseAndSymptoms(List<DiaseaseAndSymptom> diaseaseAndSymptoms) {
		this.diaseaseAndSymptoms = diaseaseAndSymptoms;
	}

	@JsonBackReference
	public List<Review> getReviews() {
		return reviews;
	}

	@JsonBackReference
	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}
	
	
	
}
