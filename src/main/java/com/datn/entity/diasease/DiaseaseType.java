package com.datn.entity.diasease;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="diaseaseType")
public class DiaseaseType {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="diaseaseTypeName")
	private String diaseaseTypeName;
	
	@JsonBackReference
	@OneToMany(mappedBy="diaseaseType")
	private List<Diasease> diaseases;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDiaseaseTypeName() {
		return diaseaseTypeName;
	}

	public void setDiaseaseTypeName(String diaseaseTypeName) {
		this.diaseaseTypeName = diaseaseTypeName;
	}

	@JsonBackReference
	public List<Diasease> getDiaseases() {
		return diaseases;
	}

	@JsonBackReference
	public void setDiaseases(List<Diasease> diaseases) {
		this.diaseases = diaseases;
	}

	
}
