package com.datn.entity.diasease;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.datn.entity.symptom.SymptomValue;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="diaseaseAndSymptomLine")
public class DiaseaseAndSymptomLine {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "diaseaseAndSymptomId")
	private DiaseaseAndSymptom diaseaseAndSymptom;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "symptomValueId")
	private SymptomValue symptomValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonManagedReference
	public DiaseaseAndSymptom getDiaseaseAndSymptom() {
		return diaseaseAndSymptom;
	}
	
	@JsonManagedReference
	public void setDiaseaseAndSymptom(DiaseaseAndSymptom diaseaseAndSymptom) {
		this.diaseaseAndSymptom = diaseaseAndSymptom;
	}
	
	@JsonManagedReference
	public SymptomValue getSymptomValue() {
		return symptomValue;
	}
	
	@JsonManagedReference
	public void setSymptomValue(SymptomValue symptomValue) {
		this.symptomValue = symptomValue;
	}
	
	
}
