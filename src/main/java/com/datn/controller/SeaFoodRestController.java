package com.datn.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.SeaFood;
import com.datn.service.seafood.ISeaFoodService;

@RestController
@RequestMapping("/api")
public class SeaFoodRestController {
	@Autowired
	private ISeaFoodService seaFoodService;
	
	@GetMapping("/seaFoods")
	public List<SeaFood> getAll() {
		return seaFoodService.getAll();
	}
	
	@GetMapping("/seaFoods/3")
	public SeaFood getById() {
		Optional<SeaFood> s = seaFoodService.getById(3);
		if(s.isPresent()) {
			SeaFood se = s.get();
			return se;
		}
		return null;
	}
	
}
