package com.datn.controller.diasease.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.diasease.DiaseaseAndSymptom;
import com.datn.repository.diasease.DiaseaseAndSymptomRepository;
import com.datn.service.diasease.IDiaseaseAndSymptomService;

@RestController
@RequestMapping("/api")
public class DiaseaseAndSymptomRestController {
	@Autowired
	private DiaseaseAndSymptomRepository repo;
	
	@Autowired
	private IDiaseaseAndSymptomService diaseaseAndSymptomService;
	
	@GetMapping("/getById1")
	public List<DiaseaseAndSymptom> getBySeaFoodId() {
		return diaseaseAndSymptomService.getBySeaFoodId(1);
	}
	
}
