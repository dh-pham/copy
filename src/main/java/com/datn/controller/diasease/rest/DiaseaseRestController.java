package com.datn.controller.diasease.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.diasease.Diasease;
import com.datn.entity.review.Review;
import com.datn.service.diasease.IDiaseaseService;

@RestController
@RequestMapping("/api")
public class DiaseaseRestController {
	@Autowired
	private IDiaseaseService diaseaseService;

	@RequestMapping(value = "/diaseases", method = RequestMethod.GET)
	public Page<Diasease> searchByKeyword(@RequestParam(name = "q", defaultValue = "all") String keyword,
			@RequestParam(name = "page", defaultValue = "0") Integer pageIndex) {
		Pageable pageable = PageRequest.of(pageIndex, 10);
		if (keyword.equals("all")) {
			return diaseaseService.getAll(pageable);
		}
		return diaseaseService.searchByKeyword(keyword, pageable);
	}

	@RequestMapping(value = "/diaseases/{id}", method = RequestMethod.GET)
	public Diasease getById(@PathVariable("id") Integer id) {
		Optional<Diasease> diaseaseOptional = diaseaseService.getById(id);
		if (diaseaseOptional.isPresent()) {
			return diaseaseOptional.get();
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/diaseases/{diaseaseId}/reviews", method = RequestMethod.GET)
	public Page<Review> getReviews(@PathVariable("diaseaseId") Integer diaseaseId,
			@RequestParam(name = "page", defaultValue = "0") Integer pageIndex) {
		Pageable pageable = PageRequest.of(pageIndex, 10);
		return diaseaseService.getReviews(diaseaseId, pageable);
	}
}
