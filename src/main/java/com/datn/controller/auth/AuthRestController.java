package com.datn.controller.auth;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.auth.AppUser;
import com.datn.service.auth.IUserDetailsService;
import com.datn.utils.JsonUtil;
import com.datn.utils.auth.CookieUtil;
import com.datn.utils.auth.EncrytedPasswordUtil;
import com.fasterxml.jackson.databind.JsonNode;

import net.minidev.json.JSONObject;

@RestController
public class AuthRestController {
	@Autowired
	private IUserDetailsService userService;

	@PostMapping("/signup")
	public ResponseEntity<String> signUpAction(@RequestBody String requestJson, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse) {

		try {
			JsonNode jsonNode = JsonUtil.getJsonNodeFromStr(requestJson);
			String yourname = jsonNode.get("yourname").asText();
			String email = jsonNode.get("email").asText();
			String password = jsonNode.get("password").asText();
			// TODO :validate sent data in server-side
			// ....
			// check if email existed
			if (userService.findByEmail(email) == null) {
				AppUser appUser = new AppUser(yourname, email, EncrytedPasswordUtil.encryptPassword(password));
				appUser.setImage("/img/profile/default.png");
				AppUser savedResult = userService.save(appUser);
				String responseStr = JsonUtil.convertObjToStr(savedResult);
				return new ResponseEntity<String>(responseStr, HttpStatus.OK);
			} else {
				JSONObject responseJson = new JSONObject();
				responseJson.put("email", "Email này đã được đăng ký bởi tài khoản khác.");
				return new ResponseEntity<String>(responseJson.toJSONString(), HttpStatus.CONFLICT);
			}

		} catch (IOException e) {
			JSONObject responseJson = new JSONObject();
			responseJson.put("errorMessage", "Server internal error");
			ResponseEntity<String> response = new ResponseEntity<String>(responseJson.toJSONString(), HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}

	}
	
	
//	@PostMapping(value = "/signin")
//	public ResponseEntity<String> signIn(@RequestBody JsonNode userNode, HttpServletRequest servletRequest,
//			HttpServletResponse servletResponse) {
//		String email = userNode.path("email").asText();
//		String password = userNode.path("password").asText();
//		AppUser appUser = userService.findByEmail(email);
//		if (appUser == null) {
//			return new ResponseEntity<String>("* Email or password is incorrect.", HttpStatus.FORBIDDEN);
//		}
//		String currentEncryptPass = appUser.getEncryptedPassword();
//		if (EncrytedPasswordUtil.matches(password, currentEncryptPass)) {
//			Cookie uidCookie = CookieUtil.generateNewCookie("uid", appUser.getId().toString(), 24 * 60 * 60);
//			servletResponse.addCookie(uidCookie);
//			return new ResponseEntity<String>("Login successfully!", HttpStatus.OK);
//		} else {
//			return new ResponseEntity<String>("* Email or password is incorrect.", HttpStatus.FORBIDDEN);
//		}
//	}


}
