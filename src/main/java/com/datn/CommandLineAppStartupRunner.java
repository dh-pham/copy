package com.datn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.datn.repository.auth.AppRoleRepository;
import com.datn.service.auth.IUserDetailsService;
import com.datn.service.classifier.ClassifierService;
import com.datn.service.seafood.ISeaFoodService;
import com.datn.service.symptom.ISymptomService;
import com.datn.service.symptom.ISymptomTypeService;
import com.datn.utils.OtherUtil;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
	@Autowired
	private ISymptomTypeService symptomTypeService;
	
	@Autowired
	private ClassifierService classifierService;
	
	@Autowired
	private IUserDetailsService iUserDetailsService;
	
	@Autowired
	private ISeaFoodService seaFoodService;
	
	@Autowired
	private AppRoleRepository roleRepo;
	
	@Autowired
	private ISymptomService symptomService;

	private static final Logger logger = LoggerFactory.getLogger(CommandLineRunner.class);

	@Override
	public void run(String... args) throws Exception {
//		classifierService.forcusRenewDataAndArff();
		classifierService.loadDataOrCreateIfNotExisted();
//		classifierService.evaluating();
//		classifierService.evaluatingAdvance();
		
	}
}